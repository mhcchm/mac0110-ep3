/***** MAC0110 - EP3 *****/
  Nome: Marcos Henrique Castello
  NUSP: 11443210

/***** Parte 1 - Entendendo o código *****/

    1) Qual a probabilidade de um elemento da matriz ser grama (ou terreno)? Você pode expressar essa possibilidade utilizando um valor numérico ou uma fórmula.

        probabilidade_terreno = 1 - (probabilidade_lobo + probabilidade_coelho + probabilidade_comida) = 1 - 0.35 = 0.65 = 65%.

    2) Analise o código e diga: qual a diferença entre o terreno e o terreno especial?

        o terreno especial é mais raro.

    3) Dados os valores iniciais das constantes, qual a energia necessária para um lobo se reproduzir? E um coelho?

        Para um lobo se reproduzir ele precisa de pelo menos fator_reprodução * energia_lobo = 2*10 = 20 unidades de energia.

/***** Parte 2 - Completando as funções *****/

    4) Ao contrário das funções ocupa_vizinho! e reproduz!, a função morre! não recebe a matriz de energia como parâmetro. Por quê não é necessário alterar a energia com a morte de um animal?

        Quando um animal morre sua energia já é zero.

/***** Parte 3 - Teste e simulação *****/

    5) Usando os valores iniciais das constantes e após uma quantidade considerável de iterações, mais de 30, por exemplo, o que acontece na ilha?

        Há uma alta chance de os lobos terem dominado a ilha, ou seja, não houver mais coelhos.

    6) Qual combinação de constantes leva a ilha a ser dominada por coelhos? Utilize uma ilha de tamanho 20 ou mais, e simule utilizando uma quantidade considerável de iterações.

        Um aumento da probabilidade_coelho e uma diminuição da probabilidade_lobo e do fator_reprodução fazem nos valores certos fazem com que a ilha seja dominada por coelhos. Por exemplo, para probabilidade_coelho = 0.7 e probabilidade_lobo = 0.0005, a ilha passa a ser dominada por coelhos na grande maioria dos casos em 50 iterações.

    7) Qual combinação de constantes leva a ilha não ter nenhum animal? Utilize uma ilha de tamanho 20 ou mais, e simule utilizando uma quantidade considerável de iterações.

        Uma diminuição do fator_reprodução faz com que a vida acabe mais rápido. Por exemplo, para fator_reprodução = 1, na grande maioria dos casos a ilha não possui mais nenhum animal depois de 50 iterações.

/***** Parte 3 - Usando DataFrames e plotando gráficos *****/

    8) Qual a diferença entre a função simula - da parte 3 - e a função simula2?

        A função simula2 gera um dataframe com o tamanho da ilha, a energia total e a quantidade de comida, lobos e coelhos.

    9) A função gera_graficos possui sintaxes diferentes das vistas em aula e nos outros exercícios. Apesar disso, é possível entender o que a função faz, sem rodá-la e sem conhecer detalhes sobre os pacotes de gráficos. Sem rodar a função, responda: quantos gráficos a função plota? Qual o conteúdo de cada gráfico?

        A função plota dois gráficos: Um de quantidade de lobos por quantidade de coelhos e outro de quantidade de energia_total por quantidade de comida.

    10) Usando os valores iniciais das constantes e após uma quantidade considerável de iterações, mais de 30, por exemplo, o que acontece nos gráficos mostrados? Houve alguma semelhança ou diferença no que você respondeu na parte 3?

        No gráfico de quantidade de lobos por quantidade de coelhos os lobos dominam a ilha rapidamente e então seu número passa a diminuir, semelhante à resposta da parte 3. No gráfico de energia total por quantidade de comida ambos diminuem enquanto a quantidade de lobos e coelhos na ilha é grande, mas depois voltam a crescer conforme estas vão diminuindo.

    11) Usando a combinação de constantes da questão 6 - que leva a ilha a ser dominada por coelhos - o que acontece nos gráficos mostrados? Houve alguma semelhança ou diferença no que você respondeu naquela questão?

        A quantidade de coelhos se mantém muito maior que a de lobos durante as primeiras iterações, mas essa diferença vai diminuindo. Além disso, a energia total diminui rapidamente devido ao decrescimento do número de animais na ilha. Tais fatos são semelhantes à resposta dada na questão.

    12) Usando a combinação de constantes da questão 7 - que leva a ilha à extinção de todos os animais - o que acontece nos gráficos mostrados? Houve alguma semelhança ou diferença no que você respondeu naquela questão?

        No gráfico de quantidade de lobos por quantidade de coelhos, o número de animais aumenta rapidamente no início das iterações, porém depois diminui rapidamente também. No gráfico de energia total por quantidade de comida, ambos diminuem conforme a quantidade de animais aumenta, mas com a queda no número de animais e depois com a extinção destes, tanto a energia total quanto a quantidade de comida crescem. Esses acontecimentos são semelhantes ao previstos pela resposta da questão.
